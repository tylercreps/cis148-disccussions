/*
    Name: Tyler Creps
    Date: 2018-10-24

    This application will demonstrate the creation of a collection of objects.
    There will be user input to create individual widget objects that contain
    a name field and a price.

    Team member with branch:
 */

public class WidgetFactory {

    public static void main(String[] args) {
        Widget[] widgets = new Widget[Widget.getMaxWidgets()];




        /*
            - Setup a Scanner object to read user input
            - Use a loop to
                  - prompt the user to input a widget name and price
                  - store values in a new widget
                  - add the widget to a widget array
                  - update the widget count
            - You can only create up to the maximum number of widgets allowed
            - Display the collection of widgets and the average price

         */

        Widget widget = new Widget();
        Widget widget2 = new Widget("Gadget", 10.5);

        widgets[0] = widget; Widget.updateCount();
        widgets[1] = widget2;  Widget.updateCount();
        widgets[2] = new Widget();  Widget.updateCount();
        widgets[3] = new Widget("Whatsit", 5.75); Widget.updateCount();

        for(Widget w : widgets) {
            System.out.println(w);

        }

        for (int i = 0; i < Widget.getCount(); ++i){
            System.out.println(widgets[i]);

        }




    }
}

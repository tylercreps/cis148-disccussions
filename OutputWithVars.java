/*
Program Author: Tyler Creps
Completed on: 8/30/18
*\
 */
import java.util.Scanner;

public class OutputWithVars {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        int userNum;
        int squareNum;
        int cubeNum;
        int userNum2;
        int addNum;
        int multNum;

        System.out.println("Enter integer:");
        userNum = scnr.nextInt();
        cubeNum = userNum * userNum * userNum;
        squareNum = userNum * userNum;
        System.out.println("You entered: " + userNum);
        System.out.println(userNum + " squared is " + squareNum);
        System.out.println("And " + userNum + " cubed is " + cubeNum + "!!");

        System.out.println("Enter another integer:");
        userNum2 = scnr.nextInt();
        addNum = userNum + userNum2;
        multNum = userNum * userNum2;
        System.out.println(userNum + " + " + userNum2 + " is " + addNum);
        System.out.println(userNum + " * " + userNum2 + " is " + multNum);

    }
}
/*
   Name: Kevin Viel
   Date: 12Sep2018
*/

import java.util.Scanner;
import java.util.Random;


public class BasicInput {
   public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in);
      Random randNum = new Random();
        /* (1) Prompt the user to input an integer, a double, a character, and a string, storing each
               into separate variables. Then, output those four values on a single line separated by
               a space. (2 pts)
        */
      int    userInt    ;
      double userDouble ;
      char   userChar   ;
      String userString ;
      int randGen = randNum.nextInt();

      /* (1) */
      System.out.println( "Enter integer:" ) ;
      userInt = scnr.nextInt() ;
      System.out.println( "Enter double:" ) ;
      userDouble = scnr.nextDouble() ;
      System.out.println( "Enter character:" ) ;
      userChar = scnr.next().charAt( 0 ) ;
      System.out.println( "Enter string:" ) ;
      userString = scnr.next();

      System.out.println( userInt
              + " "
              + userDouble
              + " "
              + userChar
              + " "
              + userString
      ) ;

      /* 2) Extend to also output in reverse. (1 pt) */
      System.out.println( userString
              + " "
              + userChar
              + " "
              + userDouble
              + " "
              + userInt
      ) ;

      /* (3) Extend to cast the double to an integer, and output that integer. (2 pts) */
      // KRV the casting is done in the output, not in a (new) variable.
      System.out.println( userDouble
              + " cast to an integer is "
              + ( int ) userDouble
      ) ;
      System.out.println(randGen);



   }
}